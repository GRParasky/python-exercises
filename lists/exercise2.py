def multiplicate_indexes_of_an_list_with_numbers(list_with_numbers):
    result_of_multiplication = 1
    for number in list_with_numbers:
        result_of_multiplication *= number
    return result_of_multiplication


result_of_multiplication_of_numbers_in_the_list = multiplicate_indexes_of_an_list_with_numbers([1, 8, 9, 20])
print(result_of_multiplication_of_numbers_in_the_list)
