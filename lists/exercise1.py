def sum_indexes_of_an_list_with_numbers(list_with_numbers):
    result_of_sum = sum(list_with_numbers)
    return result_of_sum


result_of_sum_of_numbers_in_the_list = sum_indexes_of_an_list_with_numbers([1, 2, 3])
print(result_of_sum_of_numbers_in_the_list)
