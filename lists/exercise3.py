def get_largest_number_in_a_list_of_numbers(list_of_numbers):
    largest_number = max(list_of_numbers)
    return largest_number


largest_number_in_the_list_of_numbers = get_largest_number_in_a_list_of_numbers([10, 5, 2, 20, 50, 80, 2, 30, 6, 15, 25])
print(largest_number_in_the_list_of_numbers)
