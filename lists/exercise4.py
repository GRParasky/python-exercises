def get_smallest_number_in_a_list_of_numbers(list_of_numbers):
    smallest_number = min(list_of_numbers)
    return smallest_number


smallest_number_in_the_list_of_numbers = get_smallest_number_in_a_list_of_numbers([10, 5, 2, 20, 50, 80, 1, 30, 6, 15, 25])
print(smallest_number_in_the_list_of_numbers)
