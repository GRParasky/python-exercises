def last_element(n: str):
    return n[-1]


def sort_list_of_tuples_by_the_last_element_in_each_tuple(list_of_tuples: list):
    return sorted(list_of_tuples, key=last_element)


list_of_tuples_to_be_sorted = [(3, 2), (1, 1), (2, 4), (5, 9)]
sorted_list_of_tuples = sort_list_of_tuples_by_the_last_element_in_each_tuple(list_of_tuples_to_be_sorted)
print(sorted_list_of_tuples)
