def count_specific_string_in_a_list_of_strings(list_of_strings):
    number_of_strings_that_starts_and_finishes_with_the_same_letter_and_is_bigger_or_equals_than_two = 0

    for string in list_of_strings:
        if len(string) >= 2 and string[0] == string[-1]:
            number_of_strings_that_starts_and_finishes_with_the_same_letter_and_is_bigger_or_equals_than_two += 1
    return number_of_strings_that_starts_and_finishes_with_the_same_letter_and_is_bigger_or_equals_than_two


number_of_strings_that_matches = count_specific_string_in_a_list_of_strings(["aba", "baa", "abba", "zxyz", "a", "aa"])
print(number_of_strings_that_matches)
