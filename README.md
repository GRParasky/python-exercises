# Exercícios de Python

- Exercícios sobre Listas

    - Exercício 1<br>
    `Escreva um programa em Python para somar todos os itens de uma lista.`
    
    - Exercício 2<br>
    `Escreva um programa em Python para multiplicar todos os itens de uma lista.`
    
    - Exercício 3<br>
    `Escreva um programa em Python para achar o maior número de uma lista.`
    
    - Exercício 4<br>
    `Escreva um programa em Python para achar o menor número de uma lista.`
    
    - Exercício 5<br>
        ``` 
        Escreva um programa em Python para contar o número de 
        strings de uma dada lista de strings em que a string 
        tem um tamanho igual ou maior que 2 e em que o 
        primeiro e o último caracter são iguais.
    
        Exemplo de lista : ['abc', 'xyz', 'aba', '1221']
        Resultado esperado : 2  
        ```

    - Exercício 6<br>
        ```
        Escreva um programa em Python para mostrar uma lista,
        organizada em ordem crescente pelo último elemento de
        cada tupla de uma dada lista não-vazia de tuplas
      
        Exemplo de lista : [(2, 5), (1, 2), (4, 4), (2, 3), (2, 1)]
        Resultado esperado : [(2, 1), (1, 2), (2, 3), (4, 4), (2, 5)]
        ```

